package lib

type SenecWrapperOutput struct {
	StatisticOutput StatisticOutput `json:"statistic"`
	EnergyOutput    EnergyOutput    `json:"energy"`
}

type StatisticOutput struct {
	CurrentState  string `json:"current_state"`
	LivePvGen     string `json:"live_pv_gen"`
	LiveBatCharge string `json:"live_bat_charge"`
}

type EnergyOutput struct {
	GuiGridPow           float32 `json:"gui_grid_pow"`
	GuiHousePow          float32 `json:"gui_house_pow"`
	GuiBatDataCollected  uint32  `json:"gui_bat_data_collected"`
	GuiBatDataPower      float32 `json:"gui_bat_data_power"`
	GuiInverterPower     float32 `json:"gui_inverter_power"`
	GuiBatDataFuelCharge float32 `json:"gui_bat_data_fuel_charge"`
}
