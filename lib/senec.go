package lib

import (
	"strconv"
	"strings"
	"unsafe"
)

type SenecWrapper struct {
	Statistic Statistic `json:"statistic"`
	Energy    Energy    `json:"energy"`
	Failed    bool
}

type Statistic struct {
	CurrentState  string `json:"current_state"`
	LivePvGen     string `json:"live_pv_gen"`
	LiveBatCharge string `json:"live_bat_charge"`
}

type Energy struct {
	GuiGridPow           string `json:"gui_grid_pow"`
	GuiHousePow          string `json:"gui_house_pow"`
	GuiBatDataCollected  string `json:"gui_bat_data_collected"`
	GuiBatDataPower      string `json:"gui_bat_data_power"`
	GuiInverterPower     string `json:"gui_inverter_power"`
	GuiBatDataFuelCharge string `json:"gui_bat_data_fuel_charge"`
}

func (s *SenecWrapper) Output() SenecWrapperOutput {
	var out SenecWrapperOutput
	out.EnergyOutput.GuiGridPow = convertHexToFloat32(s.Energy.GuiGridPow)
	out.EnergyOutput.GuiHousePow = convertHexToFloat32(s.Energy.GuiHousePow)
	out.EnergyOutput.GuiBatDataCollected = convertHexToInt32(s.Energy.GuiBatDataCollected)
	out.EnergyOutput.GuiBatDataPower = convertHexToFloat32(s.Energy.GuiBatDataPower)
	out.EnergyOutput.GuiInverterPower = convertHexToFloat32(s.Energy.GuiInverterPower)
	out.EnergyOutput.GuiBatDataFuelCharge = convertHexToFloat32(s.Energy.GuiBatDataFuelCharge)
	return out
}

func convertHexToFloat32(hex string) float32 {
	s := strings.Split(hex, "_")
	n, err := strconv.ParseUint(s[1], 16, 32)
	if err != nil {
		panic(err)
	}
	n2 := uint32(n)
	f := *(*float32)(unsafe.Pointer(&n2))

	return f
}

func convertHexToInt32(hex string) uint32 {
	s := strings.Split(hex, "_")
	n, err := strconv.ParseUint(s[1], 16, 32)
	if err != nil {
		panic(err)
	}
	n2 := uint32(n)

	return n2
}
