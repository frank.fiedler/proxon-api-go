package main

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"net/http"
	"os"

	"frank.fiedler/metrics-api/lib"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func getStatistics(c *gin.Context) {
	senecData := getDataFromSenec()
	c.IndentedJSON(http.StatusOK, senecData.Output())

}

func getDataFromWaterpumpShelly() lib.WaterpumpShellyWrapper {
	geturl := env("SHELLY_WASSERSTAND_INPUT_ENDPOINT")

	body := []byte(``)

	r, err := http.NewRequest("GET", geturl, bytes.NewBuffer(body))
	if err != nil {
		print(err)
		panic(err)
	}

	r.Header.Add("Content-Type", "application/json")
	client := &http.Client{}
	res, err := client.Do(r)
	if err != nil {
		panic(err)
	}

	defer res.Body.Close()

	waterpumpWrapper := &lib.WaterpumpShellyWrapper{}
	derr := json.NewDecoder(res.Body).Decode(waterpumpWrapper)
	if derr != nil {
		panic(derr)
	}

	if res.StatusCode != http.StatusOK {
		panic(res)
	}

	return *waterpumpWrapper
}

func getDataFromSenec() lib.SenecWrapper {
	posturl := env("SENEC_ENDPOINT")

	body := []byte(`{ "STATISTIC": {}, "ENERGY": {} }`)

	r, err := http.NewRequest("POST", posturl, bytes.NewBuffer(body))
	if err != nil {
		panic(err)
	}

	r.Header.Add("Content-Type", "application/json")

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}

	client := &http.Client{Transport: tr}
	res, err := client.Do(r)
	if err != nil {
		ret := lib.SenecWrapper{}
		ret.Failed = true
		return ret
		// panic(err)
	}

	defer res.Body.Close()

	senecWrapper := &lib.SenecWrapper{}
	derr := json.NewDecoder(res.Body).Decode(senecWrapper)
	if derr != nil {
		panic(derr)
	}

	if res.StatusCode != http.StatusOK {
		panic(res.Status)
	}

	senecWrapper.Failed = false

	return *senecWrapper
}

func loadDotEnv() {
	// load .env file
	godotenv.Load(".env.local", ".env")
}

func env(key string) string {
	return os.Getenv(key)
}

var reg = prometheus.NewRegistry()

var gui_grid_pow = prometheus.NewGauge(prometheus.GaugeOpts{
	Namespace: "ffiedler",
	Subsystem: "energy_monitor",
	Name:      "gui_grid_pow",
	Help:      "Current PV statistics",
})

var gui_bat_data_power = prometheus.NewGauge(prometheus.GaugeOpts{
	Namespace: "ffiedler",
	Subsystem: "energy_monitor",
	Name:      "gui_bat_data_power",
	Help:      "Current PV statistics",
})
var gui_house_pow = prometheus.NewGauge(prometheus.GaugeOpts{
	Namespace: "ffiedler",
	Subsystem: "energy_monitor",
	Name:      "gui_house_pow",
	Help:      "Current PV statistics",
})
var gui_inverter_power = prometheus.NewGauge(prometheus.GaugeOpts{
	Namespace: "ffiedler",
	Subsystem: "energy_monitor",
	Name:      "gui_inverter_power",
	Help:      "Current PV statistics",
})
var gui_bat_data_fuel_charge = prometheus.NewGauge(prometheus.GaugeOpts{
	Namespace: "ffiedler",
	Subsystem: "energy_monitor",
	Name:      "gui_bat_data_fuel_charge",
	Help:      "Current PV statistics",
})
var waterpump_power = prometheus.NewGauge(prometheus.GaugeOpts{
	Namespace: "ffiedler",
	Subsystem: "waterlevel",
	Name:      "waterpump_shelly_power",
	Help:      "Current Power Shelly (waterpump)",
})

func getMetricsHandler() gin.HandlerFunc {
	// Create a new registry.
	// reg := prometheus.NewRegistry()
	reg.MustRegister(gui_grid_pow)
	reg.MustRegister(gui_bat_data_power)
	reg.MustRegister(gui_house_pow)
	reg.MustRegister(gui_inverter_power)
	reg.MustRegister(gui_bat_data_fuel_charge)
	reg.MustRegister(waterpump_power)
	metricsHandler := promhttp.HandlerFor(
		reg,
		promhttp.HandlerOpts{
			// Opt into OpenMetrics to support exemplars.
			EnableOpenMetrics: false,
		},
	)

	// fmt.Println(senecWrapper.Energy.GuiInverterPower)
	return gin.WrapH(metricsHandler)
}

func PrometheusMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		senecWrapper := getDataFromSenec()
		waterpumpWrapper := getDataFromWaterpumpShelly()

		if senecWrapper.Failed == false {
			gui_inverter_power.Set(float64(senecWrapper.Output().EnergyOutput.GuiInverterPower))
			gui_grid_pow.Set(float64(senecWrapper.Output().EnergyOutput.GuiGridPow))
			gui_bat_data_power.Set(float64(senecWrapper.Output().EnergyOutput.GuiBatDataPower))
			gui_house_pow.Set(float64(senecWrapper.Output().EnergyOutput.GuiHousePow))
			gui_bat_data_fuel_charge.Set(float64(senecWrapper.Output().EnergyOutput.GuiBatDataFuelCharge))
		}

		waterpump_power.Set(float64(waterpumpWrapper.Power))
		c.Next()
	}
}

func main() {
	loadDotEnv()
	router := gin.Default()
	router.Use(PrometheusMiddleware())
	router.GET("/stats", getStatistics)
	router.GET("/metrics", getMetricsHandler())
	router.Run("0.0.0.0:" + env("SERVER_PORT"))
}
