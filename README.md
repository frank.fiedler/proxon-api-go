# metrics-api-go

docker build -t metrics-api .

docker run -it -p 8080:8080 --rm --name metrics-api-running metrics-api
or
docker compose up

curl --location '${HOST}:${PORT}/metrics'